<!DOCTYPE html>
<html>
<head>
  <title>Raspberry pi Status Page</title>

  <!-- jQuery and jQuery UI (REQUIRED) -->
  <link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

  <!-- elFinder CSS (REQUIRED) -->
  <link rel="stylesheet" type="text/css" href="/elFinder/css/elfinder.min.css">
  <link rel="stylesheet" type="text/css" href="/elFinder/css/theme.css">

  <!-- elFinder JS (REQUIRED) -->
  <script src="/elFinder/js/elfinder.min.js"></script>
  <script type="text/javascript" charset="utf-8">
  // Documentation for client options:
  // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
  $(document).ready(function() {
    var options = {
      url  : 'elFinder/php/connector.minimal.php',
      lang : 'en',
      sync : 1100,
      defaultView : 'list',
      resizable : false,
      rememberLastDir : false,
      useBrowserHistory : false,
      uiOptions : {
        // toolbar configuration
        toolbar : [
          // ['reload'],
          // ['home', 'up'],
          ['upload'],
          ['download', 'getfile'],
          ['info'],
          ['quicklook'],
          ['rm'],
          ['rename', 'resize'],
          ['extract', 'archive'],
          ['search'],
          ['view'],
        ],

        // directories tree options
        tree : {
          // expand current root on init
          openRootOnLoad : true,
          // auto load current dir parents
          syncTree : true
        },

        // navbar options
        navbar : {
          minWidth : 150,
          maxWidth : 500
        },

        // current working directory options
        cwd : {
          // display parent directory in listing as ".."
          oldSchool : false
        }
      }

    }
    $('#elfinder').elfinder(options);
  });

  $('#contactForm').submit(function () {
    sendContactForm();
    return false;
  });
  </script>

  <?php
  // Global variables
  $sds_run_mode_value = trim(shell_exec("sudo sed -n 's/^.*sds_run_mode=//p' /opt/scripts/sds.cfg"));
  $sds_dmb_mode_value = trim(shell_exec("sudo sed -n 's/^.*sds_dmb_mode=//p' /opt/scripts/sds.cfg"));
  $sds_webbrowser_value = trim(shell_exec("sudo sed -n 's/^.*sds_webbrowser=//p' /opt/scripts/sds.cfg"));
  $sds_image_change_value = trim(shell_exec("sudo sed -n 's/^.*sds_image_change=//p' /opt/scripts/sds.cfg"));
  $sds_webbrowser_reload_value = trim(shell_exec("sudo sed -n 's/^.*sds_webbrowser_reload=//p' /opt/scripts/sds.cfg"));
  $day_part1_start_value = trim(shell_exec("sudo sed -n 's/^.*day_part1_start=//p' /opt/scripts/sds.cfg"));
  $day_part2_start_value = trim(shell_exec("sudo sed -n 's/^.*day_part2_start=//p' /opt/scripts/sds.cfg"));
  $day_part3_start_value = trim(shell_exec("sudo sed -n 's/^.*day_part3_start=//p' /opt/scripts/sds.cfg"));
  $day_part4_start_value = trim(shell_exec("sudo sed -n 's/^.*day_part4_start=//p' /opt/scripts/sds.cfg"));
  $day_part5_start_value = trim(shell_exec("sudo sed -n 's/^.*day_part5_start=//p' /opt/scripts/sds.cfg"));
  $sds_webpage_address_value = trim(shell_exec("sudo sed -n 's/^.*webpage_address=//p' /opt/scripts/sds.cfg"));

  $ip_address_value=trim(shell_exec("hostname -I | cut -d' ' -f1"));
  $redirect_url="http://$ip_address_value";
  $netmask_value=trim(shell_exec("ifconfig eth0 | grep Mask | cut -d':' -f4"));
  $gateway_value=trim(shell_exec("ip route show default | awk '/default/ {print $3}'"));
  $dns1_value=trim(shell_exec("sed -n 's/^.*nameserver//p' /etc/resolv.conf | sed '1!d'"));
  $dns2_value=trim(shell_exec("sed -n 's/^.*nameserver//p' /etc/resolv.conf | sed '2!d'"));
  $time_server_value=trim(shell_exec("sed -n 's/^.*time_server=//p' /opt/scripts/sds.cfg"));
  $timezone_value=trim(shell_exec("sed '1!d' /etc/timezone"));

  $sds_hdmi_on_time_value = trim(shell_exec("sudo sed -n 's/^.*sds_hdmi_on_time=//p' /opt/scripts/sds.cfg"));
  $sds_hdmi_off_time_value = trim(shell_exec("sudo sed -n 's/^.*sds_hdmi_off_time=//p' /opt/scripts/sds.cfg"));

  $resolution_value = trim(shell_exec("sudo sed -n 's/^hdmi_mode=//p' /boot/config.txt"));
  $rotation_value = trim(shell_exec("sudo sed -n 's/^display_rotate=//p' /boot/config.txt"));
  $hdmi_state_value = trim(shell_exec("sudo tvservice -s | grep HDMI | wc -l"));
  $network_static_true = trim(shell_exec("sed -n 's/^.*static.*/1/p' /etc/network/interfaces"));

  /**
  * Timezones list with GMT offset
  *
  * @return array
  * @link http://stackoverflow.com/a/9328760
  */
  function tz_list() {
    $zones_array = array();
    $timestamp = time();
    foreach(timezone_identifiers_list() as $key => $zone) {
      date_default_timezone_set($zone);
      $zones_array[$key]['zone'] = $zone;
      $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
    }
    return $zones_array;
  }

  ?>

  <script>

  function toggleDiv() {
    var div = document.getElementById('info_div');
    if (div.style.display !== 'none'){
      div.style.display = 'none';;
    }
    else{
      div.style.display = 'block';
    }
  }

  function toggleDiv2() {
    var div = document.getElementById('network_div');
    if (div.style.display !== 'none'){
      div.style.display = 'none';;
    }
    else{
      div.style.display = 'block';
    }
  }

  function Submit_RPI_Network()
  {
    document.getElementById("network_config_").submit();
    <?php
    if (isset($_POST['save_rpi_network']))
    {
      //Network Configuration
      $file_array = array();
      $file_array[] = 'auto lo' . PHP_EOL;
      $file_array[] = 'iface lo inet loopback' . PHP_EOL;
      $file_array[] = '' . PHP_EOL;
      $file_array[] = '#auto eth0' . PHP_EOL;
      $file_array[] = '#allow-hotplug eth0' . PHP_EOL;
      $file_array[] = 'iface eth0 inet' . " " . $_POST['network_mode'] .  PHP_EOL;
      if (($_POST['network_mode']) === 'static')
      {
        $file_array[] = 'address' . " " . $_POST['ip_address'] . PHP_EOL;
        $file_array[] = 'netmask' . " " . $_POST['netmask'] . PHP_EOL;
        $file_array[] = 'gateway' . " " . $_POST['gateway'] . PHP_EOL;
      }
      file_put_contents("/tmp/interfaces_tmp", $file_array);
      $file_array1 = array();
      $file_array1[] = 'nameserver' . " " . $_POST['dns1'] . PHP_EOL;
      $file_array1[] = 'nameserver' . " " . $_POST['dns2'] . PHP_EOL;
      file_put_contents("/tmp/resolv.conf_tmp", $file_array1);
      shell_exec('sudo -u root cp /tmp/interfaces_tmp /etc/network/interfaces ');
      shell_exec('sudo -u root cp /tmp/resolv.conf_tmp /etc/resolv.conf ');
      $file_array2 = array();
      $file_array2[] = $_POST['timezone'] .  PHP_EOL;
      if (($_POST['timezone']) != '0' ){
        file_put_contents("/tmp/timezone_tmp", $file_array2);
        shell_exec('sudo -u root cp /tmp/timezone_tmp /etc/timezone ');
      }
      shell_exec("sudo sed -i 's/^time_server=.*$/time_server=".$_POST['time_server']."/' /opt/scripts/sds.cfg");
      header("Location: $redirect_url");

    }
    ?>

  }

  function Submit_RPI_Config()
  {
    document.getElementById("rpi_config").submit();
    <?php
    if (isset($_POST['save_rpi_config']))
    {
      //shell_exec("sudo sed -i 's/^.*sds_run_mode=.*$/sds_run_mode=".$_POST['amdisplay_mode']."/' /opt/scripts/sds.cfg");
      if ($_POST['amdisplay_mode'] =='fusion'){
        shell_exec('sudo -u root /tmp/set fusion 2>&1');
      }
      elseif ($_POST['amdisplay_mode'] =='wkd'){
        shell_exec('sudo -u root /tmp/set wkd 2>&1');
      }
      elseif ($_POST['amdisplay_mode'] =='dmb'){
        shell_exec('sudo -u root /tmp/set dmb 2>&1');
      }
      //shell_exec("sudo sed -i 's/^.*sds_dmb_mode=.*$/sds_dmb_mode=".$_POST['dmb_mode']."/' /opt/scripts/sds.cfg");
      if ($_POST['dmb_mode'] =='AllWeek'){
        shell_exec('sudo -u root /tmp/set allweek 2>&1');
      }
      elseif ($_POST['dmb_mode'] =='DayPart'){
        shell_exec('sudo -u root /tmp/set daypart 2>&1');
      }
      elseif ($_POST['dmb_mode'] =='WeekPart'){
        shell_exec('sudo -u root /tmp/set weekpart 2>&1');
      }
      //shell_exec("sudo sed -i 's/^hdmi_mode=.*$/hdmi_mode=".$_POST['resolution']."/' /boot/config.txt");
      if ($_POST['resolution'] =='auto'){
        shell_exec('sudo -u root /tmp/set autop 2>&1');
      }
      elseif ($_POST['resolution'] =='4'){
        shell_exec('sudo -u root /tmp/set 720p 2>&1');
      }
      elseif ($_POST['resolution'] =='16'){
        shell_exec('sudo -u root /tmp/set 1080p 2>&1');
      }
      //shell_exec("sudo sed -i 's/^display_rotate=.*$/display_rotate=".$_POST['rotate']."/' /boot/config.txt");
      if ($_POST['rotate'] =='0'){
        shell_exec('sudo -u root /tmp/set rotate0 2>&1');
      }
      elseif ($_POST['rotate'] =='1'){
        shell_exec('sudo -u root /tmp/set rotate90 2>&1');
      }
      elseif ($_POST['rotate'] =='2'){
        shell_exec('sudo -u root /tmp/set rotate180 2>&1');
      }
      elseif ($_POST['rotate'] =='3'){
        shell_exec('sudo -u root /tmp/set rotate270 2>&1');
      }
      if($sds_run_mode_value == 'dmb'){
        //DMB time and reload settings
        shell_exec("sudo sed -i 's/^sds_image_change=.*$/sds_image_change=".$_POST['image_change']."/' /opt/scripts/sds.cfg");
        //shell_exec("sudo sed -i 's/^day_part1_start=.*$/day_part1_start=".$_POST['day_part1_start_time']."/' /opt/scripts/sds.cfg");
        shell_exec("sudo sed -i 's/^day_part2_start=.*$/day_part2_start=".$_POST['day_part2_start_time']."/' /opt/scripts/sds.cfg");
        shell_exec("sudo sed -i 's/^day_part3_start=.*$/day_part3_start=".$_POST['day_part3_start_time']."/' /opt/scripts/sds.cfg");
        shell_exec("sudo sed -i 's/^day_part4_start=.*$/day_part4_start=".$_POST['day_part4_start_time']."/' /opt/scripts/sds.cfg");
        shell_exec("sudo sed -i 's/^day_part5_start=.*$/day_part5_start=".$_POST['day_part5_start_time']."/' /opt/scripts/sds.cfg");
      }
      if(isset($_POST['browser_select'])){
        shell_exec("sudo sed -i 's/^.*sds_webbrowser=.*$/sds_webbrowser=".$_POST['browser_select']."/' /opt/scripts/sds.cfg");
      }
      //Webbrowser reolad settings
      if($sds_run_mode_value != 'dmb'){
        shell_exec("sudo sed -i 's/^sds_webbrowser_reload=.*$/sds_webbrowser_reload=".$_POST['webbrowser_reload']."/' /opt/scripts/sds.cfg");
        //Cron settings
        $file_array3 = array();
        $file_array3[] = '0 0 1 * * find /var/log -size +5120k -exec rm {} \;' . PHP_EOL;
          $file_array3[] = '*/5 * * * * /tmp/./autostart' . PHP_EOL;
          $file_array3[] = '*/5 * * * * /tmp/./set screenshot' . PHP_EOL;
          $file_array3[] = '1 0 * * * /tmp/./set auto_hdmi' . PHP_EOL;
          $file_array3[] = '2 0 * * * /tmp/./set time' . PHP_EOL;
          $file_array3[] = '* 5 * * * /tmp/./set reload' . PHP_EOL;
          $file_array3[] = '*/'. $_POST['webbrowser_reload']. ' ' . '* * * * /tmp/./set webbrowser_reload' . PHP_EOL;
          file_put_contents("/tmp/cron_tmp", $file_array3);
          shell_exec("sudo -u root crontab -r");
          shell_exec("sudo -u root crontab /tmp/cron_tmp");
        }
        //HDMI settings
        if(($_POST['hdmi'] === 'on') and ($hdmi_state_value === '0')){
          shell_exec('sudo -u root /tmp/./set hdmi_on 2>&1');
        }
        elseif(($_POST['hdmi'] === 'off') and ($hdmi_state_value === '1')){
          shell_exec('sudo -u root /tmp/./set hdmi_off 2>&1');
        }
        if($_POST['hdmi_on_time']!= "$sds_hdmi_on_time_value"){
          shell_exec("sudo sed -i 's/^sds_hdmi_on_time=.*$/sds_hdmi_on_time=".$_POST['hdmi_on_time']."/' /opt/scripts/sds.cfg");
        }
        elseif($_POST['hdmi_off_time']!= "$sds_hdmi_off_time_value"){
          shell_exec("sudo sed -i 's/^sds_hdmi_off_time=.*$/sds_hdmi_off_time=".$_POST['hdmi_off_time']."/' /opt/scripts/sds.cfg");
        }

        //Copy config to tmp
        shell_exec("sudo cp /opt/scripts/sds.cfg /tmp/sds.cfg");
        // Autostart after changes
        shell_exec('sudo -u root /tmp/./autostart 2>&1');

        header("Location: $redirect_url");
      }
      ?>

    }
    function Submit_RPI_Webpage_Address()
    {
      document.getElementById("rpi_webpage_address").submit();
      <?php
      if(isset($_POST['save_rpi_webpage_address'])) {
        shell_exec("sudo sed -i 's,.*webpage_address=.*,webpage_address=" . $_POST['webpage_address'] . ",' /opt/scripts/sds.cfg");
        //Copy config to tmp
        shell_exec("sudo cp /opt/scripts/sds.cfg /tmp/sds.cfg");
        header("Location: $redirect_url");
      }
      ?>
    }
    //not used
    function RPI_Reboot()
    {
      var div = document.getElementById('count_div');
      if (div.style.display !== 'none'){
        div.style.display = 'none';;
      }
      else{
        div.style.display = 'block';
      }

      $.get("reboot.php");
      o=document.getElementById('sekundy')
      function odliczaj(o,sek){
        o.innerHTML=sek
        if(sek>0)setTimeout(function(){odliczaj(o,--sek)},1e3)
      }
      odliczaj(document.getElementById('sekundy'),27)
      setTimeout(
        function() {
          location.reload();
        }, 27000)
      }

      </script>


      <style type="text/css">
      body {
        background-image: url(/images/wood-repeat.jpg);
        background-repeat: repeat;
      }
      p {
        color: green;
        font-size: 16px;
      }
      pre {
        font-family: "Courier New", Courier, monospace;
        font-size: 14px;
      }
      table {
        border-collapse:separate;
        border-radius:6px;
        -moz-border-radius:6px;
      }

      input[type=text] {
        border: 2px solid blue;
        border-radius: 4px;
      }

      #content_wkd{
        border-radius:6px;
        -moz-border-radius:6px;
        padding-top: 1px;
      }
      .titlebar {
        font-size: xx-large;
        font-weight: bold;
        text-align: center;
      }
      .preheader {
        font-size: small;
        font-weight: bold;
        text-align: center;
      }
      .button{
        text-decoration:none;
        text-align:center;
        padding:6px 8px;
        border:solid 1px #004F72;
        -webkit-border-radius:4px;
        -moz-border-radius:4px;
        border-radius: 4px;
        font:10px Arial, Helvetica, sans-serif;
        font-weight:bold;
        color:#000000;
        background-color:#3BA4C7;
        background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%);
        background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%);
        background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%);
        background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 );
        background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%);
        -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;
        -moz-box-shadow: 0px 0px 2px #bababa,  inset 0px 0px 1px #ffffff;
        box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;
      }
      .button_reboot{
        text-decoration:none;
        text-align:center;
        padding:6px 8px;
        border:solid 1px #004F72;
        -webkit-border-radius:4px;
        -moz-border-radius:4px;
        border-radius: 4px;
        font:10px Arial, Helvetica, sans-serif;
        font-weight:bold;
        color:#000000;
        background-color:#ff0000;
        background-image: -moz-linear-gradient(top, #ff0000 0%, #a54c19 100%);
        background-image: -webkit-linear-gradient(top, #ff0000 0%, #a54c19 100%);
        background-image: -o-linear-gradient(top, #ff0000 0%, #a54c19 100%);
        background-image: -ms-linear-gradient(top, #ff0000 0% ,#a54c19 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a54c19', endColorstr='#a54c19',GradientType=0 );
        background-image: linear-gradient(top, #ff0000 0% ,#a54c19 100%);
        -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;
        -moz-box-shadow: 0px 0px 2px #bababa,  inset 0px 0px 1px #ffffff;
        box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;
      }
      .button_save{
        text-decoration:none;
        text-align:center;
        padding:6px 8px;
        border:solid 1px #004F72;
        -webkit-border-radius:4px;
        -moz-border-radius:4px;
        border-radius: 4px;
        font:10px Arial, Helvetica, sans-serif;
        font-weight:bold;
        color:#000000;
        background-color:#2bff00;
        background-image: -moz-linear-gradient(top, #2bff00 0%, #19a531 100%);
        background-image: -webkit-linear-gradient(top, #2bff00 0%, #19a531 100%);
        background-image: -o-linear-gradient(top, #2bff00 0%, #19a531 100%);
        background-image: -ms-linear-gradient(top, #2bff00 0% ,#19a531 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#19a531', endColorstr='#19a531',GradientType=0 );
        background-image: linear-gradient(top, #2bff00 0% ,#19a531 100%);
        -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;
        -moz-box-shadow: 0px 0px 2px #bababa,  inset 0px 0px 1px #ffffff;
        box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;
      }

      </style>
    </head>

    <?php
    // buttons
    if (isset($_POST['screenshot']))
    {
      shell_exec('sudo -u root /tmp/set screenshot 2>&1');
    }
    elseif (isset($_POST['reload']))
    {
      shell_exec('sudo -u root /tmp/set reload 2>&1');
      shell_exec('sudo -u root /tmp/set screenshot 2>&1');
    }
    elseif (isset($_POST['poweroff']))
    {
      shell_exec('sudo -u root poweroff 2>&1');
    }
    elseif (isset($_POST['factory_config']))
    {
      shell_exec('sudo -u root /tmp/set initial 2>&1');
      header("Location: $redirect_url");
    }
    elseif (isset($_POST['extend_root']))
    {
      shell_exec('sudo -u root raspi-config --expand-rootfs 2>&1');
      header("Location: $redirect_url");
    }
    ?>


    <body>
      <center>

        <img src="/tmp/AmDisplay.png?<?=Date('U')?>" alt="Screenshot" <?php if (($rotation_value != '1' ) and ($rotation_value != '3')) {echo "style='width:480px;height:270px;'";} else {echo "style='width:270px;height:480px;'";}?> ><br><br>
          <div id="timestamp"  style='width: 600px;background-color: rgb(211,211,211); border-radius: 6px;'>

            <h2>Last Screenshot
              <?php
              $filename = '/tmp/AmDisplay.png';
              if (file_exists($filename)) {
                echo "Timestamp: " . date ("F d Y H:i:s.", filemtime($filename));
              }
              echo "<br> Current IP Address: ";
              echo shell_exec("hostname -I | cut -d' ' -f1     ");
              ?>
              <button style="cursor: pointer" type="button" class="btn btn-default" onclick="toggleDiv2()"><span>&#9881;</span></button>
            </h2>
            <div id="network_div"   style="display:none;">
              <form action="#" name="network_config_form" id="network_config" method="post" >
                <input type="radio"  name="network_mode" <?php if ($network_static_true == '1') {echo "checked";}?> value="static" ><strong>STATIC</strong></button>
                <input type="radio"  name="network_mode" <?php if ($network_static_true != '1') {echo "checked";}?> value="dhcp" ><strong>DHCP</strong></button><br><br>
                <strong>IP address:</strong> <input type="text" name="ip_address"  size="10" maxlength="15" value='<?php echo"$ip_address_value"?>'><br>
                <strong>Subnet mask:</strong> <input type="text" name="netmask"  size="10" maxlength="15" value='<?php echo"$netmask_value"?>'><br>
                <strong>Gateway:</strong> <input type="text" name="gateway"  size="10" maxlength="15" value='<?php echo"$gateway_value"?>'><br>
                <strong>DNS1:</strong> <input type="text" name="dns1"  size="10" maxlength="15" value='<?php echo"$dns1_value"?>'><br>
                <strong>DNS2:</strong> <input type="text" name="dns2"  size="10" maxlength="15" value='<?php echo"$dns2_value"?>'><br><br>
                <strong>Time Settings:</strong><br>
                <strong>Time Server:</strong> <input type="text" name="time_server"  size="10" maxlength="15" value='<?php echo"$time_server_value"?>'><br>
                <strong>Current TimeZone:</strong> <?php echo"$timezone_value"?><br>
                <select name="timezone" style="width: 300px;">
                  <option value="0">Please, select correct timezone</option>
                  <?php foreach(tz_list() as $t) { ?>
                    <option value="<?php print $t['zone'] ?>">
                      <?php print $t['diff_from_GMT'] . ' - ' . $t['zone'] ?>
                    </option>
                    <?php } ?>
                  </select><br><br>
                  <button class="button_save" name="save_rpi_network" onclick="Submit_RPI_Network()">Save Settings</button><br>
                </form>
              </div>
              <div id="count_div"  style="display:none; color:#d9d9d9;" >
                <p>Auto reload in: </p>
                <span style="font-size:xx-large; color:red;" id="sekundy"></span>
              </div>
            </div>
          </center>

          <table width="800px" height="320px" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#d9d9d9">
            <tr>
              <td valign="top" ; td align="center" ; width="35%"><h2>AmDisplay Mode </h2>

                <form action="#" name="rpi_config_form" id="rpi_config" method="post" >
                  <input type="radio" name="amdisplay_mode" value="fusion" >Fusion</button><?php if ($sds_run_mode_value === 'fusion') {echo "&nbsp;&#8678";}?><br>

                  <input type="radio" name="amdisplay_mode" value="wkd" >Web Kiosk Display</button><?php if ($sds_run_mode_value === 'wkd') {echo "&nbsp;&#8678";}?><br>

                  <input type="radio" name="amdisplay_mode" value="dmb" >Digital Menuboard</button><?php if ($sds_run_mode_value === 'dmb') {echo "&nbsp;&#8678";}?><br>
                  <br>

                  <div id="mode_dmb"   <?php if ($sds_run_mode_value != 'dmb'){ echo "style='display:none'"; } ?> >
                    <h2>Digital Menuboard Mode </h2>

                    <input type="radio"  name="dmb_mode"  value="AllWeek" >AllWeek</button><?php if ($sds_dmb_mode_value === 'AllWeek') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="dmb_mode"  value="DayPart" >DayPart</button><?php if ($sds_dmb_mode_value === 'DayPart') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="dmb_mode"  value="WeekPart" >WeekPart</button><?php if ($sds_dmb_mode_value === 'WeekPart') {echo "&nbsp;&#8678";}?><br>

                    Picture change (seconds) <input type="text" name="image_change"  size="1" maxlength="2" value='<?php echo"$sds_image_change_value"?>'><br><br>

                    <div id="day_parts_times" <?php if (($sds_run_mode_value != 'dmb') or ($sds_dmb_mode_value === 'AllWeek')) { echo "style='display:none'"; } ?>>
                      Day Parts Starts (HH:mm):<br><br>
                      1:<input type="text" name="day_part1_start_time"  size="4" maxlength="5" value='<?php echo"$day_part1_start_value"?>'>
                      2:<input type="text" name="day_part2_start_time"  size="4" maxlength="5" value='<?php echo"$day_part2_start_value"?>'>
                      3:<input type="text" name="day_part3_start_time"  size="4" maxlength="5" value='<?php echo"$day_part3_start_value"?>'><br>
                      4:<input type="text" name="day_part4_start_time"  size="4" maxlength="5" value='<?php echo"$day_part4_start_value"?>'>
                      5:<input type="text" name="day_part5_start_time"  size="4" maxlength="5" value='<?php echo"$day_part5_start_value"?>'>

                    </div>

                    <br>
                  </div>
                  <div id="select_browser"   <?php if ($sds_run_mode_value === 'dmb'){ echo "style='display:none'"; } ?> >
                    <h2>Select WKD browser </h2>

                    <input type="radio"  name="browser_select"  value="iceweasel" >Firefox</button><?php if ($sds_webbrowser_value === 'iceweasel') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="browser_select"  value="midori" >Midori</button><?php if ($sds_webbrowser_value === 'midori') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="browser_select"  value="epiphany" >Epiphany</button><?php if ($sds_webbrowser_value === 'epiphany') {echo "&nbsp;&#8678";}?><br>

                    WebBrowser reload (minutes) <input type="text" name="webbrowser_reload"  size="1" maxlength="2" value='<?php echo"$sds_webbrowser_reload_value"?>'>

                    <br>
                  </div>
                  <td valign="top" ; td align="center" ; width="30%" ><h2>Display Settings </h2>

                    <input type="radio"  name="resolution"  value="auto" >Auto resolution</button><?php if (($resolution_value != '4' ) and ($resolution_value != '16')) {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="resolution"  value="4" >720p resolution</button><?php if ($resolution_value === '4') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="resolution" value="16" >1080p resolution</button><?php if ($resolution_value === '16') {echo "&nbsp;&#8678";}?><br>
                    <br>
                    <input type="radio"  name="rotate"  value="0" >Rotate 0*</button><?php if ($rotation_value === '0') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="rotate"  value="1" >Rotate 90*</button><?php if ($rotation_value === '1') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="rotate"  value="2" >Rotate 180*</button><?php if ($rotation_value === '2') {echo "&nbsp;&#8678";}?><br>

                    <input type="radio"  name="rotate"  value="3" >Rotate 270*</button><?php if ($rotation_value === '3') {echo "&nbsp;&#8678";}?><br>
                    <br>
                    <input type="radio"  name="hdmi"  value="on" >HDMI on </button>
                    (HH:mm)<input type="text" name="hdmi_on_time"  size="4" maxlength="5" value='<?php echo"$sds_hdmi_on_time_value"?>'><?php if ($hdmi_state_value === '1') {echo "&nbsp;&#8678";}?>
                    <br>

                    <input type="radio"  name="hdmi" value="off" >HDMI off</button>
                    (HH:mm)<input type="text" name="hdmi_off_time"  size="4" maxlength="5" value='<?php echo"$sds_hdmi_off_time_value"?>'><?php if ($hdmi_state_value === '0') {echo "&nbsp;&#8678";}?>
                    <br>
                    <br>

                    <td valign="top" ; td align="center" ; width="35%" ><h2>Management</h2>

                      <button class="button" name="screenshot">Screenshot</button>
                      <br><br>
                      <button class="button" name="reload">Reload</button>
                      <button class="button" name="extend_root">FitSD</button>
                      <br><br>
                      <button class="button" name="factory_config">Factory RPI settings</button>
                      <br><br>
                      <button class="button_save" name="save_rpi_config" onclick="Submit_RPI_Config()">Save Configuration</button>
                      <br><br>
                      <button class="button_reboot" name="poweroff" >Shut Down</button>
                    </form>
                    <input class="button_reboot" type="button" name="reboot" value="Reboot" onclick="RPI_Reboot()" >
                    <br><br>
                    <input class="button" type="button" id='toggle_info_div' name="answer" value="Toggle RPI Info" onclick="toggleDiv()" >
                    <button class="button" onclick="window.location.href='http://<?php echo "$ip_address_value" ?>:6080/vnc.html'">VNC</button>
                    <br><br>

                  </tr>
                </table>

                <center>
                  <div id="spacer" style='height: 25px;'></div>
                  <div id="file_manager_dmb"  style='width: 800px;background:white;border-radius: 6px; <?php if ($sds_run_mode_value != 'dmb'){ echo 'display:none'; } ?>' >
                    <div id="elfinder"></div>
                  </div>

                  <div id="content_wkd" style='width: 800px; height: 200px; background:#d9d9d9; <?php if ($sds_run_mode_value === 'dmb'){ echo 'display:none';}?>'>
                    <h2>Fusion/Kioskboard Display Address</h2>
                    <pre>Fusion Example: http://192.168.192.168/yumcustomerdisplay/</pre>
                    <pre>Kioskboard Example: http://192.168.192.168/kioskboard/iframe.html</pre>

                    <form action="#" id="rpi_webpage_address" method="post">
                      <input type="text" name="webpage_address"  size="50" value='<?php echo"$sds_webpage_address_value"?>'>
                      <button class="button_save" name="save_rpi_webpage_address" onclick="Submit_RPI_Webpage_Address()">Save Address</button><br>
                      <br>
                    </form>
                  </div>


                  <?php
                  $GLOBALS['REQUEST_MICROTIME'] = microtime( TRUE );
                  function load_time( $buffer ){
                    return str_replace('{microtime}', round( microtime( TRUE ) - $GLOBALS['REQUEST_MICROTIME'], 4 ), $buffer);}
                    ob_start( 'load_time' );

                    $cpu_info = trim(shell_exec("cat /proc/cpuinfo | grep Serial | cut -d ':' -f 2"));
                    $board_ver = intval(substr($cpu_info, strpos($cpu_info, 'Revision	: ') + 11, 5));
                    $cmdline = trim(shell_exec('cat /proc/cmdline'));
                    $mem_base = substr($cmdline, strpos($cmdline, 'mem_base') + 11, 1);
                    $mem_size = substr($cmdline, strpos($cmdline, 'mem_size') + 11, 1);
                    $issue = shell_exec('cat /boot/issue.txt');
                    $issue_type = substr($issue, 0 , strpos($issue, ')') + 1);
                    ?>

                    <br>
                    <div id="info_div"  style="display:none;" >
                      <table width="800" border="1" align="center" cellpadding="3" bgcolor="#d9d9d9">
                        <tr>
                          <td valign="top"><strong>RPI INFO</strong><pre>
                            <?php
                            echo "Serial number   : $cpu_info \n";
                            if (intval(substr(shell_exec('cat /proc/cpuinfo | grep "CPU revision"'), -1, 1)) && 0x80)
                            echo 'Warranty Bit    : Bad';
                            else
                            echo 'Warranty Bit    : Good<br /><br />';
                            echo 'Memory Size     : ' . intval($mem_size) * 256 . 'M<br />';
                            echo 'Memory Split    : ';
                            switch ($mem_base) {
                              case 'f':
                              echo ((intval($mem_size) - 1) * 256) + 240 . 'M ARM / 16M GPU';
                              break;
                              case 'e':
                              echo ((intval($mem_size) - 1) * 256) + 224 . 'M ARM / 32M GPU';
                              break;
                              case 'c':
                              echo ((intval($mem_size) - 1) * 256) + 192 . 'M ARM / 64M GPU';
                              break;
                              case '8':
                              echo ((intval($mem_size) - 1) * 256) + 128 . 'M ARM / 128M GPU';
                              break;
                              default:
                              echo 'Other/User Defined';
                            }
                            echo '<br />';
                            echo '</pre><strong>UPTIME</strong><br /><pre>' . shell_exec('uptime');
                            ?>
                          </td>
                        </table>
                        <table width="800" border="1" align="center" cellpadding="3" bgcolor="#d9d9d9" >
                          <tr>

                            <td colspan="2" valign="top"><strong>HEALTH</strong><br />
                              <br />
                              <?php
                              $float_temp = round(intval(shell_exec('cat /sys/class/thermal/thermal_zone0/temp'))/1000, 1);
                              $s1 = array($float_temp);
                              ?>
                              <table width="100%" border="0">
                                <tr>
                                  <td align="center"><strong>Pi Temperature = <?php echo $float_temp  . ' C (' . round(($float_temp * 9 / 5 + 32), 1) . ' F)'; ?> </strong></td>
                                </tr>
                              </table></td>
                            </tr>

                            <tr>
                              <td colspan="2"><strong>MEMORY USAGE</strong><br />
                                <pre><?php echo shell_exec('free -o -h'); ?></pre></td>
                              </tr>
                              <tr>
                                <td colspan="2"><strong>DRIVE USAGE</strong><br />
                                  <pre><?php echo shell_exec('df -h -T'); ?></pre></td>
                                </tr>
                                <tr>
                                  <td colspan="2"><strong>USB INFO</strong><br />
                                    <pre><?php echo shell_exec('lsusb'); ?></pre></td>
                                  </tr>
                                  <tr>
                                    <td valign="top" ><strong>CMDLINE</strong><br />
                                      <pre><?php echo str_replace(" ", "\n", $cmdline); ?></pre></td>
                                    </tr>
                                    <tr>
                                      <td valign="top" ><strong>CURRENT CONFIG</strong><br />
                                        <pre><?php echo shell_exec('cat /boot/config.txt'); ?></pre></td>
                                      </tr>
                                      <td valign="top" ><strong>SDS LOG</strong><br />
                                        <pre><?php echo shell_exec('tail -10 /var/log/sds.log'); ?></pre></td>
                                      </tr>
                                    </table>
                                  </div>
                                  <br />
                                  <table width="579" border="0" align="center">
                                    <tr>
                                      <td width="503" align="center" class="preheader"><span class="preheader">
                                        <br/>Created for RPI AmDisplay by Pawel Glica 2015-2016</span></td>
                                      </tr>
                                    </table>
                                  </center>
                                  </html>
